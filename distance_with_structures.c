#include<stdio.h>
#include<math.h>

struct points
{
	float x;
	float y;
};

typedef struct points xy;

	xy input();
	float dist(xy P1, xy P2);
	float output(xy P1, xy P2, float dist);


int main()
{
	xy P1, P2;
	float distance;
	printf("Coordinate of the first point:\n");
	P1 = input();
	printf("coordinate of the second point:\n");
	P2 = input();
	distance = dist(P1, P2);
	output(P1, P2, distance);
	return 0;
}

xy input()
{
	xy P;
	printf("x coordinate:\n");
	scanf("%f", &P.x);
	printf("y coordinate:\n");
	scanf("%f", &P.y);
	return P;
}

float dist(xy P1, xy P2)
{
	return (sqrt(pow((P1.x-P2.x),2)+pow((P1.y-P2.y),2)));
}

float output(xy P1, xy P2, float dist)
{
	printf("x-y coordinate of 1st point:(%.2f, %.2f)\n", P1.x, P2.y);
	printf("x-y coordinate of 2nd point:(%.2f, %.2f)\n", P1.x, P2.y);
	printf("distance between the given points is: %.2f\n", dist);
	return 0;
}
