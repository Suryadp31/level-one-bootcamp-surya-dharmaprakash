#include <stdio.h>
float input()
{
    float num;
    printf("Enter a number\n");
    scanf("%f",&num);
    return num;
}

float sum(float n1, float n2)
{
    float sum;
    sum = n1+n2;
    return sum;
}

void output(float n1, float n2, float ans)
{
    printf("The sum of %f + %f is %f\n",n1,n2,ans);
}

int main()
{
    float x,y,z;
    x=input();
    y=input();
    z=sum(x,y);
    output(x,y,z);
    return 0;
}
//Write a program to add two user input numbers using 4 functions.