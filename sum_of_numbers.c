#include <stdio.h>
int input()
{
    int num;
    printf("Enter the number of elements for addition: \n");
    scanf("%d",&num);
    return num;
}
void arr_in(int num, int a[num])
{
    for(int i=0;i<num;i++)
    {
        printf("Enter the element no %d of the array\n",i+1);
        scanf("%d",&a[i]);
    }
}
int arr_s(int num, int a[num])
{
    int sum=0;
    for(int i=0;i<num;i++) 
    {
        sum += a[i];
    }
    return sum;
}
void output(int num, int a[num], int sum)
{
    int i;
    printf("The sum of "); 
    for(i=0;i<num-1;i++) 
    {
        printf("%d+",a[i]);
    }
    printf("%d=%d",a[i],sum);
}
int main()
{
    int num;
    int sum,a[num];
    num = input();
    arr_in(num,a);
    sum=arr_s(num,a);
    output(num,a,sum);
}
