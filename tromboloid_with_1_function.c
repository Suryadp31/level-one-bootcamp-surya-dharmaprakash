#include <stdio.h>
 
 
int main()
 
{
  float h, d, b, vol;
  printf("Enter the respective values of h, d, b:\n");
  scanf("%f %f %f", &h, &d, &b);
  vol = (float)((h*d)+d)/(3*b);
  printf("The volume of the tromboloid is with parameters h, d ,b:%f\n", vol);
 
  return 0;
}
