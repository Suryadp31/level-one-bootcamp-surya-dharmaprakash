#include<stdio.h>
#include<math.h>
float input(char coordinate, int point)
{
	float c;
	printf("enter the %c coordinate of point %d:\n", coordinate, point);
	scanf("%f", &c);
}
float dist(float x1, float y1, float x2, float y2)
{
	float distance;
	distance= sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1);
	return distance;
}
void output(float x1, float y1, float x2, float y2, float distance)
{
	printf("distance between %f, %f and %f, %f is %f\n", x1, y1, x2, y2, distance);
}
int main()
{
	float x1, y1, x2, y2, distance;
	x1=input('x', 1);
	y1=input('y', 1);
	x2=input('x', 2);
	y2=input('y', 2);
	distance = dist(x1,y1,x2,y2);
	output(x1,y1,x2,y2,distance);
return 0;
}
