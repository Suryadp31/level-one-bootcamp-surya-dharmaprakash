#include<stdio.h>
struct fraction
    {
        int numerator;
        int denominator;
    };
typedef struct fraction Frac;
Frac sum(Frac F1,Frac F2);
void print(Frac F1,Frac F2,Frac a);
Frac input()
{
    Frac n;
    printf("Enter the numerator and the denominator:\n");
    scanf("%d%d",&n.numerator,&n.denominator);
    return n;
 }
int main()
{
    Frac F1,F2;
    F1 = input();
    F2 = input();
    Frac a = sum(F1,F2);
    print(F1,F2,a);
}
int gcd(int numerator, int denominator, Frac f1, Frac f2)
{
   
    int temp, num, den;
    num = numerator;
    den = denominator;
    while(numerator!=0)
    {
        temp = numerator;
        numerator = den%numerator;
        denominator = temp;
    }
   return denominator;
}
Frac sum(Frac f1,Frac f2)
{
   Frac s;
    int num,den, numerator, denominator;
    num = ((f1.numerator* f2.denominator) + (f2.numerator * f1.denominator));
    den = (f1.denominator * f2.denominator);
    int g = gcd(num, den, f1,f2);
    s.numerator = num/g;
    s.denominator = den/g;
    return s;
    
}
void print(Frac f1,Frac f2,Frac a)
{
  printf("The sum of the fractions %d/%d and %d/%d is %d/%d",F1.numerator,f1.denominator,f2.numerator,f2.denominator,a.numerator,a.denominator);
}
